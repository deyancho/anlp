#Here are some libraries you're likely to use. You might want/need others as well.
import re
import sys
from random import random
from math import log
from collections import defaultdict, OrderedDict
import numpy as np

tri_counts=defaultdict()    #counts of all trigrams in input
big_counts=defaultdict()
probability_dist_trigrams = OrderedDict()


#this function currently does nothing.
def preprocess_line(line):
    line.rstrip()
    preprocessed_line = re.sub(r'[^a-zA-Z0-9. ]+', '', line)
    preprocessed_line = re.sub(r'[1-9]', '0', preprocessed_line).lower()
    tokens_preprocessed_line = preprocessed_line.split()
    preprocessed_line = ' '.join(tokens_preprocessed_line) # replace several white spaces with one
    return preprocessed_line

#here we make sure the user provides a training filename when
#calling this program, otherwise exit with a usage error.

print('Please type the task you want to run - 3(train a model from a corpus), 4(generate sequence from a model file) or 5(calculate perplexity):')
option = input()
if option == '3':
    print('Please type name of the training file: ')
    file_input_name = input()
    print('Please type name of the language model(output) file: ')
    lm_file_name = input()

elif option == '4':
    print('Please type name of a trigram language model file.')
    file_input_name = input()
    print('Please wait until a sequence is generated:')

elif option == '5':
    print('Please type the name of the test file:')
    test_file = input()
    print('Please type the name the language model file: ')
    model_file = input()
    print('Please specify if double hashtags in the end of trigrams (x##) probabilities are in the model[y/n](default is yes): ')
    double_hashtags = input()
    if double_hashtags == 'y':
        double_hashtags = True
    elif double_hashtags == 'n':
        double_hashtags = False
    else:
        print('Please specify using "y" or "n". Exiting.')
        sys.exit(0)

else:
    print('Invalid option. Exiting.')
    sys.exit(0)

if option == '3':

    #This bit of code gives an example of how you might extract trigram counts
    #from a file, line by line. If you plan to use or modify this code,
    #please ensure you understand what it is actually doing, especially at the
    #beginning and end of each line. Depending on how you write the rest of
    #your program, you may need to modify this code.
    with open(file_input_name) as f:
        for line in f:
            line = "##" + preprocess_line(line) + "#"
            for j in range(len(line)-(2)):
                trigram = line[j:j+3]
                if trigram not in tri_counts:
                    tri_counts[trigram] = 1
                else:
                    tri_counts[trigram] += 1
    #Some example code that prints out the counts. For small input files
    #the counts are easy to look at but for larger files you can redirect
    #to an output file (see Lab 1).
    print("Trigram counts in ", file_input_name, ", sorted alphabetically:")
    for trigram in sorted(tri_counts.keys()):
        print(trigram, ": ", tri_counts[trigram])
    print("Trigram counts in ", file_input_name, ", sorted numerically:")
    for tri_count in sorted(tri_counts.items(), key=lambda x:x[1], reverse = True):
        print(tri_count[0], ": ", str(tri_count[1]))

    # Implementation of alpha smoothing(with default value 1)
    add_alpha_constant=1

    trigram_permutations = [i + j + k for i in '#0. abcdefghijklmnopqrstuvwxyz' for j in '#0. abcdefghijklmnopqrstuvwxyz' for k in '#0. abcdefghijklmnopqrstuvwxyz']
    non_permited_trigrams = set(['#' + char + '#' for char in '#0. abcdefghijklmnopqrstuvwxyz'] + \
    [char1 + '#' + char2 for char1 in '0. abcdefghijklmnopqrstuvwxyz' for char2 in '0. abcdefghijklmnopqrstuvwxyz'])

    unseen_trigrams = list((set(trigram_permutations).difference(set(tri_counts.keys()))).difference(non_permited_trigrams))
    # print(len(unseen_trigrams))

    for u_t in unseen_trigrams:
        tri_counts[u_t] = 0

    with open(file_input_name) as f:
        for line in f:
            line = "##" + preprocess_line(line) + "#"
            for j in range(len(line)-(1)):
                bigram = line[j:j+2]
                if bigram not in big_counts:
                    big_counts[bigram] = 1
                else:
                    big_counts[bigram] += 1

    bigram_permutations = [i + j for i in '#0. abcdefghijklmnopqrstuvwxyz' for j in '#0. abcdefghijklmnopqrstuvwxyz' for k in '#0. abcdefghijklmnopqrstuvwxyz']

    unseen_bigrams = list(set(bigram_permutations).difference(set(big_counts.keys())))

    for u_t in unseen_bigrams:
        big_counts[u_t] = 0

    probabilities_trigrams = defaultdict()

    for k in tri_counts.keys():
        # probabilities_trigrams[k] = "{:.3e}".format((tri_counts[k] + add_alpha_constant)/(big_counts[k[:-1]] + add_alpha_constant*30))
        probabilities_trigrams[k] = "{:.4}".format((tri_counts[k] + add_alpha_constant)/(big_counts[k[:-1]] + add_alpha_constant*30))

    with open(lm_file_name, 'w') as file_to_write:
        sorted_keys = sorted(probabilities_trigrams.keys())
        for s_k in sorted_keys:
            prob = probabilities_trigrams[s_k]
            line_to_write = s_k + '\t' + prob + '\n'
            file_to_write.write(line_to_write)

    # print(probabilities_trigrams)
elif option == '4':
    # load file to dict
    with open(file_input_name, 'r') as model_file:
        for line in model_file:
            trigram = line[:3]
            prob_trigram = line[3:].strip()
            probability_dist_trigrams[trigram] = float(prob_trigram)

    # distribution for trigrams that start with '##'
    sequence = "##"
    probability_incrementor = 0
    distribution = []
    chars_generated = 0
    for t, p in probability_dist_trigrams.items():
        if t.startswith('##'):
            prob_start = probability_incrementor
            probability_incrementor += p
            prob_end = probability_incrementor
            distribution.append([prob_start, prob_end, t])
    sample = np.random.uniform()
    for d in distribution:
        if sample >= d[0] and sample < d[1]:
            sequence += d[2][2]
            chars_generated += 1
    # print(probability_incrementor)
    while chars_generated < 297:
        saw_hashtag = False
        hashtag_index = -1
        probability_hashtag = 0
        distribution = []
        probability_incrementor = 0
        for t, p in probability_dist_trigrams.items():
            if t.startswith(sequence[-2:]) and not t.endswith('#'):
                prob_start = probability_incrementor
                probability_incrementor += p
                prob_end = probability_incrementor
                distribution.append([prob_start, prob_end, t])
            if t.startswith(sequence[-2:]) and t.endswith('#'):
                saw_hashtag = True
                probability_hashtag = p

        if saw_hashtag:
            prob_to_distribute = probability_hashtag/len(distribution)
            for i, bucket in enumerate(distribution):
                if i==0:
                    bucket[1] += prob_to_distribute
                else:
                    bucket[0] += prob_to_distribute
                    bucket[1] += prob_to_distribute

        sample = np.random.uniform()
        for d in distribution:
            if sample >= d[0] and sample < d[1]:
                sequence += d[2][2]
                chars_generated += 1

        #print(sequence)
    sequence += '#'
    print(sequence)
# print(len(trigram_permutations))

elif option == '5':
    N = 0
    probs = []
    with open(model_file, 'r') as model_file:
        for line in model_file:
            trigram = line[:3]
            prob_trigram = line[3:].strip()
            probability_dist_trigrams[trigram] = float(prob_trigram)
    file_string = ""
    lines_no_double_hashtags = []
    with open(test_file, 'r') as test_file:
        for line in test_file:
            if double_hashtags:
                line = '#' + preprocess_line(line) + '#'
            else:
                lines_no_double_hashtags.append('##' + preprocess_line(line) + "#")
            file_string += line
    if not double_hashtags:
        for pp_line in lines_no_double_hashtags:
            for i in range(len(pp_line) - 2):
                trigram = pp_line[i:i + 3]
                probs.append(probability_dist_trigrams[trigram])
                N += 1
        log_prob = 0
        for prob in probs:
            log_prob += log(prob, 2)
        entropy = -(1.0/N)*log_prob
        PP = 2**entropy
        print("Perplexity:")
        print(PP)

    else:
        for i in range(len(file_string)-2):
            trigram = file_string[i:i+3]
            probs.append(probability_dist_trigrams[trigram])
            N+=1

        log_prob = 0
        for prob in probs:
            log_prob += log(prob, 2)
        entropy = -(1.0/N)*log_prob
        PP = 2**entropy
        print("Perplexity:")
        print(PP)
